<?php

namespace Drupal\domain_simple_sitemap\Plugin\simple_sitemap\SitemapType;

use \Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapType\SitemapTypeBase;

/**
 * Class DomainSitemapType
 * @package Drupal\domain_simple_sitemap\Plugin\simple_sitemap\SitemapType
 *
 * @SitemapType(
 *   id = "domain",
 *   label = @Translation("Domain"),
 *   description = @Translation("The domain sitemap type."),
 *   sitemapGenerator = "default",
 *   urlGenerators = {
 *     "custom",
 *     "entity",
 *     "domain_entity_node",
 *     "domain_entity_menu_link_content",
 *     "arbitrary",
 *   },
 * )
 */
class DomainSitemapType extends SitemapTypeBase {
}
