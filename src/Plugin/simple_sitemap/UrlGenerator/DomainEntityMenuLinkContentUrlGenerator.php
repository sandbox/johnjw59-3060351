<?php

namespace Drupal\domain_simple_sitemap\Plugin\simple_sitemap\UrlGenerator;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuLinkBase;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\simple_sitemap\EntityHelper;
use Drupal\simple_sitemap\Logger;
use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGeneratorBase;
use Drupal\simple_sitemap\Simplesitemap;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DomainEntityMenuLinkContentUrlGenerator
 * @package Drupal\domain_simple_sitemap\Plugin\simple_sitemap\UrlGenerator
 *
 * @UrlGenerator(
 *   id = "domain_entity_menu_link_content",
 *   label = @Translation("Domain Menu link URL generator"),
 *   description = @Translation("Generates menu link URLs by overriding the 'entity' URL generator with domains in mind."),
 *   settings = {
 *     "overrides_entity_type" = "menu_link_content",
 *   },
 * )
 */
class DomainEntityMenuLinkContentUrlGenerator extends EntityUrlGeneratorBase {

  /**
   * @var \Drupal\Core\Menu\MenuLinkTree
   */
  protected $menuLinkTree;

  /**
   * The domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * DomainEntityMenuLinkContentUrlGenerator constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Simplesitemap $generator,
    Logger $logger,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityHelper $entityHelper,
    MenuLinkTree $menu_link_tree,
    DomainNegotiatorInterface $domain_negotiator
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $generator,
      $logger,
      $language_manager,
      $entity_type_manager,
      $entityHelper
    );
    $this->menuLinkTree = $menu_link_tree;
    $this->domainNegotiator = $domain_negotiator;
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_sitemap.generator'),
      $container->get('simple_sitemap.logger'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('simple_sitemap.entity_helper'),
      $container->get('menu.link_tree'),
      $container->get('domain.negotiator')
    );
  }

  /**
   * @inheritdoc
   */
  public function getDataSets() {
    $data_sets = [];
    $bundle_settings = $this->generator
      ->setVariants($this->sitemapVariant)
      ->getBundleSettings();

    if (!empty($bundle_settings['menu_link_content'])) {
      foreach ($bundle_settings['menu_link_content'] as $bundle_name => $bundle_settings) {
        if (!empty($bundle_settings['index'])) {
          $menu_link_storage = $this->entityTypeManager->getStorage('menu_link_content');

          // Retrieve the expanded tree.
          $tree = $this->menuLinkTree->load($bundle_name, new MenuTreeParameters());
          $tree = $this->menuLinkTree->transform($tree, [
            ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
            ['callable' => 'menu.default_tree_manipulators:flatten'],
          ]);

          foreach ($tree as $i => $item) {
            $current_domain = FALSE;
            $uuid = $item->link->getDerivativeId();

            // Only add menu link content entities if they're on this domain.
            if (!empty($uuid)) {
              $entity = array_values($menu_link_storage->loadByProperties(['uuid' => $uuid]))[0];

              if ($entity->{DOMAIN_ACCESS_ALL_FIELD}->value) {
                $current_domain = TRUE;
              }
              else {
                foreach($entity->{DOMAIN_ACCESS_FIELD} as $field) {
                  if ($field->target_id == $this->sitemapVariant) {
                    $current_domain = TRUE;
                    break;
                  }
                }
              }
            }

            if ($current_domain) {
              $data_sets[] = $item->link;
            }
          }
        }
      }
    }

    return $data_sets;
  }

  /**
   * @inheritdoc
   *
   * @todo Check if menu link has not been deleted after the queue has been built.
   */
  protected function processDataSet($data_set) {
    /** @var  MenuLinkBase $data_set */
    if (!$data_set->isEnabled()) {
      return FALSE;
    }

    $url_object = $data_set->getUrlObject();

    // Do not include external paths.
    if ($url_object->isExternal()) {
      return FALSE;
    }

    $meta_data = $data_set->getMetaData();
    // If not a menu_link_content link, use bundle settings.
    if (empty($meta_data['entity_id'])) {
      $entity_settings = $this->generator
        ->setVariants($this->sitemapVariant)
        ->getBundleSettings('menu_link_content', $data_set->getMenuName());
    }
    else {
      // If menu link is of entity type menu_link_content, take under account its entity override.
      $entity_settings = $this->generator
        ->setVariants($this->sitemapVariant)
        ->getEntityInstanceSettings('menu_link_content', $meta_data['entity_id']);

      if (empty($entity_settings['index'])) {
        return FALSE;
      }
    }

    if ($url_object->isRouted()) {
      // Do not include paths that have no URL.
      if (in_array($url_object->getRouteName(), ['<nolink>', '<none>'])) {
        return FALSE;
      }

      $path = $url_object->toUriString();
    }
    else {
      // There can be internal paths that are not rooted, like 'base:path'.
      if (strpos($uri = $url_object->toUriString(), 'base:') === 0) {
        $path = substr($uri, 5);
      }
      else {
         // Handle unforeseen schemes.
        $path = $uri;
      }
    }

    $url_object->setOption('absolute', TRUE);

    $entity = $this->entityHelper->getEntityFromUrlObject($url_object);

    $path_data = [
      'url' => $url_object,
      'lastmod' => !empty($entity) && method_exists($entity, 'getChangedTime')
        ? date('c', $entity->getChangedTime())
        : NULL,
      'priority' => isset($entity_settings['priority']) ? $entity_settings['priority'] : NULL,
      'changefreq' => !empty($entity_settings['changefreq']) ? $entity_settings['changefreq'] : NULL,
      'images' => !empty($entity_settings['include_images']) && !empty($entity)
        ? $this->getEntityImageData($entity)
        : [],
      'meta' => [
        'path' => $path,
      ]
    ];
    if (!empty($entity)) {
      $path_data['meta']['entity_info'] = [
        'entity_type' => $entity->getEntityTypeId(),
        'id' => $entity->id(),
      ];
    }

    return $path_data;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($data_set) {
    // Set the current domain to this variants domain so access checks pass.
    if ($this->domainNegotiator->getActiveId() !== $this->sitemapVariant) {
      if ($domain = $this->entityTypeManager->getStorage('domain')->load($this->sitemapVariant)) {
        $this->domainNegotiator->setActiveDomain($domain);
      }
    }
    return parent::generate($data_set);
  }

}
