<?php

namespace Drupal\domain_simple_sitemap\Plugin\simple_sitemap\UrlGenerator;

use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\simple_sitemap\EntityHelper;
use Drupal\simple_sitemap\Logger;
use Drupal\simple_sitemap\Simplesitemap;
use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGeneratorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DomainEntityNodeUrlGenerator.
 * @package Drupal\domain_simple_sitemap\Plugin\simple_sitemap\UrlGenerator
 *
 * @UrlGenerator(
 *   id = "domain_entity_node",
 *   title = @Translation("Domain Node URL generator"),
 *   description = @Translation("Generates URLs for nodes with domains in mind."),
 *   settings = {
 *     "overrides_entity_type" = "node",
 *   },
 * )
 */
class DomainEntityNodeUrlGenerator extends EntityUrlGeneratorBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * DomainEntityNodeUrlGenerator constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Simplesitemap $generator,
    Logger $logger,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityHelper $entityHelper,
    Connection $connection,
    DomainNegotiatorInterface $domain_negotiator
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $generator,
      $logger,
      $language_manager,
      $entity_type_manager,
      $entityHelper
    );
    $this->connection = $connection;
    $this->domainNegotiator = $domain_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_sitemap.generator'),
      $container->get('simple_sitemap.logger'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('simple_sitemap.entity_helper'),
      $container->get('database'),
      $container->get('domain.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDataSets() {
    $data_sets = [];
    $bundle_settings = $this->generator
      ->setVariants($this->sitemapVariant)
      ->getBundleSettings();

    if (!empty($bundle_settings['node'])) {
      foreach ($bundle_settings['node'] as $type => $bundle_settings) {
        if (!empty($bundle_settings['index'])) {
          $query = $this->entityTypeManager->getStorage('node')->getQuery()
            ->condition('type', $type)
            ->condition('status', 1);

          // Only grab nodes available on this variant's domain.
          $orGroupDomain = $query->orConditionGroup()
            ->condition(DOMAIN_ACCESS_FIELD . '.target_id', $this->sitemapVariant)
            ->condition(DOMAIN_ACCESS_ALL_FIELD, 1);
          $query->condition($orGroupDomain);

          $data_sets = array_merge($data_sets, array_values($query->execute()));
        }
      }
    }

    return $data_sets;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataSet($nid) {
    if (empty($node = $this->entityTypeManager->getStorage('node')->load($nid))) {
      return FALSE;
    }

    $entity_settings = $this->generator
      ->setVariants($this->sitemapVariant)
      ->getEntityInstanceSettings('node', $nid);

    if (empty($entity_settings['index'])) {
      return FALSE;
    }

    $url_object = $node->toUrl();

    // Do not include external paths.
    if (!$url_object->isRouted()) {
      return FALSE;
    }

    // We're using the path alias here so it plays nicer with menu links.
    $path = substr($url_object->toString(), 1);

    $url_object->setOption('absolute', TRUE);

    return [
      'url' => $url_object,
      'lastmod' => date('c', $node->getChangedTime()),
      'priority' => isset($entity_settings['priority']) ? $entity_settings['priority'] : NULL,
      'changefreq' => !empty($entity_settings['changefreq']) ? $entity_settings['changefreq'] : NULL,
      'images' => !empty($entity_settings['include_images'])
        ? $this->getEntityImageData($node)
        : [],
      'meta' => [
        'path' => $path,
        'entity_info' => [
          'entity_type' => 'node',
          'id' => $nid,
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function generate($data_set) {
    // Set the current domain to this variants domain so access checks pass.
    if ($this->domainNegotiator->getActiveId() !== $this->sitemapVariant) {
      if ($domain = $this->entityTypeManager->getStorage('domain')->load($this->sitemapVariant)) {
        $this->domainNegotiator->setActiveDomain($domain);
      }
    }
    return parent::generate($data_set);
  }

}
