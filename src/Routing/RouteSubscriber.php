<?php

namespace Drupal\domain_simple_sitemap\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Use a domain based default sitemap controller.
    if ($route = $collection->get('simple_sitemap.sitemap_default')) {
      $route->setDefaults(array(
        '_controller' => '\Drupal\domain_simple_sitemap\Controller\DomainSitemapController::getCurrrentSitemap',
      ));
    }
  }

}
