<?php

namespace Drupal\domain_simple_sitemap\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\simple_sitemap\Simplesitemap;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DomainSitemapController.
 */
class DomainSitemapController extends ControllerBase {

  /**
   * The Simple Sitemap generator.
   *
   * @var \Drupal\simple_sitemap\Simplesitemap
   */
  protected $sitemapGenerator;

  /**
   * The Domain Negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Constructs a new DomainSitemapController object.
   */
  public function __construct(Simplesitemap $sitemap_generator, DomainNegotiatorInterface $domain_negotiator) {
    $this->sitemapGenerator = $sitemap_generator;
    $this->domainNegotiator = $domain_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_sitemap.generator'),
      $container->get('domain.negotiator')
    );
  }

  /**
   * Returns the whole sitemap, the requested chunk, or the sitemap index file
   * for the variant corresponding to the current domain.
   *
   * @see \Drupal\simple_sitemap\Controller\SimplesitemapController::getSitemap()
   *
   * @param Request $request
   *  The request object.
   *
   * @throws NotFoundHttpException
   *
   * @return Response|false
   *  Returns an XML response.
   */
  public function getCurrrentSitemap(Request $request) {
    $variant = $this->domainNegotiator->getActiveId();
    $output = $this->sitemapGenerator->setVariants($variant)->getSitemap($request->query->getInt('page'));
    if (!$output) {
      throw new NotFoundHttpException();
    }

    return new Response($output, Response::HTTP_OK, [
      'Content-type' => 'application/xml; charset=utf-8',
      'X-Robots-Tag' => 'noindex, follow',
    ]);
  }

}
